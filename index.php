<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8"/>
    <link href="css/base.css" rel="stylesheet"/>
    <link href="css/index.css" rel="stylesheet"/>
    <title>Voyages</title>
</head>

<body>
<header>
    <div class="voyage">
        <h1>
            <strong><a href="index.php">Voyage</a></strong>
        </h1>
        <p>Voyager pour partager</p>
    </div>
    <div class="picture">
        <p>
            <img alt="Photo voyage" src="images/header.jpg" />
        </p>
    </div>
    <div class="research">
        <form method="post" action="recherche_traitement.php">
            <p>
                <input type="search" name="recherche" id="recherche" placeholder="Votre recherche ..."/><br />
            </p>
        </form>
    </div>

</header>

<nav>
    <ul>
        <li><a href="index.php">Accueil</a></li>
        <li><a href="tools.php">Outils</a></li>
        <li><a href="trip.php">Voyages</a></li>
        <li><a href="sign_up.php">Inscription</a></li>
        <li><a href="sign_in.php">Connexion</a></li>
        <li><a href="contact.php">Contact</a></li>
    </ul>
</nav>

<section>
    <div class="trip_search">
        <h1>Chercher un voyage</h1>
    </div>
    <div class="trip_propose">
        <h1>Proposer un voyage</h1>
    </div>
    <div class="trip_france">
        <h1>Voyager en France</h1>
    </div>
    <div class="trip_abroad">
        <h1>Voyager à l'étranger</h1>
    </div>
</section>

<footer>
    <h5>Voyager pour partage</h5>
    <div class="bottomFooter">
        <a href="index.php">Accueil</a> /
        <a href="tools.php">Outils</a> /
        <a href="trip.php">Voyages</a> /
        <a href="sign_up.php">Inscription</a> /
        <a href="sign_in.php">Connexion</a> /
        <a href="contact.php">Contact</a>
    </div>
</footer>
</body>
</html>